import argparse
import shutil
from os.path import dirname, join

from packagetools.builder import *
from packagetools.command import cygwin_path, run

VERSION = "origin/woa"
RECIPE_DIR = dirname(__file__)


class Recipe(PackageShortRecipe):
    def __init__(self, revision):
        self.revision = revision

    def describe(self) -> PackageInfo:
        return PackageInfo(
            description="blender",
            id="blender",
            pretty_name="Blender",
            version="woa",
        )

    def all_steps(self, out_dir: str):
        bash = shutil.which("bash")
        recipe = cygwin_path(join(RECIPE_DIR, "recipe.sh"))
        run(bash, recipe, self.revision, cygwin_path(out_dir))


parser = argparse.ArgumentParser(description="Build blender")
parser.add_argument("--revision", help="Revision to build", default=VERSION)
args = parser.parse_args()
PackageBuilder(Recipe(args.revision)).make()
