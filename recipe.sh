#!/usr/bin/env bash

set -euo pipefail
set -x

die()
{
    echo "$@" >&1
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_folder"
revision=$1; shift
out_folder=$1; shift

git_for_windows()
{
    # Git for windows is needed because blender build system check git root dir.
    # MSYS2 version answers /c/... instead of C:/
    export PATH=$(pwd)/git_for_windows/cmd:$PATH
    if [ -d git_for_windows ]; then return; fi

    mkdir git_for_windows
    pushd git_for_windows
    wget https://github.com/git-for-windows/git/releases/download/v2.43.0.windows.1/MinGit-2.43.0-64-bit.zip
    wget https://github.com/git-lfs/git-lfs/releases/download/v3.4.1/git-lfs-windows-arm64-v3.4.1.zip
    unzip MinGit*.zip
    unzip git-lfs*.zip
    cp git-lfs-*/git-lfs.exe cmd/git-lfs.exe
    rm *.zip
    popd

}

checkout()
{
    which git
    if [ -d blender ]; then return; fi
    git clone https://gitlab.com/Linaro/windowsonarm/forks/blender
    pushd blender
    git checkout $revision
    git log -n1
    # GIT_TERMINAL_PROMPT allows submodules to fallback on official blender repos
    env GIT_TERMINAL_PROMPT=0 python build_files/utils/make_update.py --no-libraries --no-blender
    popd
}

set_build_path()
{
    BUILD_PATH="$PATH"

    set +x
    # blender deps refuse to build if pythonw is on PATH
    # https://github.com/blender/blender/blob/3f6d1d098c9b068ade44ea6b5e640b338a42fc89/build_files/build_environment/windows/build_deps.cmd#L56
    while [ "$(env PATH="$BUILD_PATH" which pythonw 2>/dev/null)" != "" ]; do
        pythonw="$(env PATH="$BUILD_PATH" which pythonw)"
        dir=$(dirname "$pythonw")
        echo "Remove python dir '$dir' from PATH"
        BUILD_PATH=$(echo $BUILD_PATH |
                     sed -e "s#:$dir/\?:#:#Ig" -e "s#:$dir/Scripts/\?:#:#Ig")
    done
    set -x
}

build_deps()
{
    pushd blender

    # blender deps use C:/t as temp folder
    # https://github.com/blender/blender/blob/1905667967debea1d40c0a42e7fce2c76536195a/build_files/build_environment/windows/build_deps.cmd#L71
    # override this
    temp_folder=/c/blender_tmp/
    rm -rf $temp_folder
    mkdir -p $temp_folder
    sed -i -e "s#set TMPDIR.*#set TMPDIR=$(cygpath -m $temp_folder)#" build_files/build_environment/windows/build_deps.cmd

    cat > build_deps.cmd << EOF
@rem MSYS2_ARG_CONV_EXCL some deps break if not set
set MSYS2_ARG_CONV_EXCL=
call build_files/build_environment/windows/build_deps.cmd 2022 arm64 || exit 1
EOF

    # build deps with modified path
    env PATH="$BUILD_PATH" cmd.exe /c "build_deps.cmd"

    rm -rf $temp_folder

    popd

    # copy deps in lib folder
    mkdir -p lib/
    rsync -av blender/build/output/WinArm64_vc15/ blender/lib/windows_arm64
    
    # create a fake .git folder to fool the blender build system
    mkdir blender/lib/windows_arm64/.git

    # copy missing headers from blender svn
    pushd blender/lib/windows_arm64
    if [ ! -d wintab ]; then
        mkdir -p wintab/include
        pushd wintab/include
        wget https://svn.blender.org/svnroot/bf-blender/trunk/lib/win64_vc15/wintab/include/wintab.h
        wget https://svn.blender.org/svnroot/bf-blender/trunk/lib/win64_vc15/wintab/include/pktdef.h
        popd
    fi
    popd
}

build()
{
    if [ "${BLENDER_BUILD_DEPS_ONLY:-}" == 1 ]; then return; fi

    pushd blender
    cat > build.cmd << EOF
set MSYS2_ARG_CONV_EXCL=
call make.bat release verbose 2022 arm64 ninja || exit 1
EOF
    env PATH="$BUILD_PATH" cmd.exe /c "build.cmd"
    popd
}

package()
{
    mkdir -p $out_folder
    rsync -av build_windows_Release_arm64_vc17_Release/bin/ $out_folder/
}

git_for_windows
set_build_path
checkout
build_deps
build
package
